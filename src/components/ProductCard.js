import { Card, Col, Container } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { useState } from 'react';
import {Link} from 'react-router-dom';


export default function ProductCard({productProp}){

	const { _id, name, price, image } = productProp;

	return (
			<Col className = "col-4 mt-5 colcourse">
				<Card as = {Link} to = {`/products/${_id}`} className = "h-100 ms-5 me-5 text-center justify-content card">
			    	<Card.Body className="d-flex flex-column justify-content">
			    		<Card.Img variant="top" src={image} className="img-fluid mb-3 ms-auto me-auto" style={{ width: '250px', height: '250px' }}/>
			        </Card.Body>
			        <Card.Footer className="bg footer card-footer text-fluid d-flex flex-column justify-content-between align-items-stretch" style={{ minHeight: '100px' }}> 
			        	<Container fluid className="d-flex flex-column h-50">
			        		<Card.Title className = "text-center text-light title mt-auto">{name}</Card.Title>
			        	</Container>
			        	<Container fluid className="d-flex flex-column mt-auto h-50">
			        		<Card.Subtitle className="mt-auto text-light mb-2">Price: ${price}</Card.Subtitle>
			        	</Container>
			        </Card.Footer>
				</Card>
			</Col>
	)
}

// Check if the CourseCard component is getting the correct prop types
// Proptypes are used for validating information passed to a component and is a tool normally used to help developers ensure the correct information is being passed from one component to the next
ProductCard.propTypes = { 
	// The "shape" method is used to check if a prop object conforms to a specific shape
	courseProp: PropTypes.shape({
		name: PropTypes.string.isRequired,
		description: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired
	})
}