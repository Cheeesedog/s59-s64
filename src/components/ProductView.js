import { Row, Col, Button, Card, Container, Form } from 'react-bootstrap';
// The use params allows us to get or extract the parameter included in our pages
import {useParams, useNavigate, Link} from 'react-router-dom';

import UserContext from '../UserContext';

import {useEffect, useState, useContext} from 'react' 

import Swal2 from 'sweetalert2';

export default function ProductView(){

	const [name, setName] = useState('');
	const [price, setPrice] = useState('');
	const [description, setDescription] = useState('');
	const [image, setImage] = useState('');
	const [quantity, setQuantity] = useState(1);


	const {id} = useParams();

	const {user} = useContext(UserContext);

	const navigate = useNavigate();

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
		.then(response => response.json())
		.then(data => {
			// console.log(data);
			setName(data.name);
			setPrice(data.price);
			setDescription(data.description);
			setImage(data.image);

		})
	}, []);

	useEffect(() => {

		if(quantity === 0){
			setQuantity(1);
		}

	}, [quantity])

	function checkout(event){
		event.preventDefault();

			fetch(`${process.env.REACT_APP_API_URL}/users/checkout`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: id,
				quantity: quantity
			})
		})
		.then(response => response.json())
		.then(data => {
			// console.log(data);
			if(data === true){
				Swal2.fire({
				title: 'Purchase successful',
				icon: 'success',
				text: 'Successfully purchased an item!',
				confirmButtonText: 'OK',
				confirmButtonColor: '#2a454e'
			}).then(() => {
				navigate('/products');
			})

			} else {
				Swal2.fire({
					title: 'Purchase unsuccessful',
					icon: 'error',
					text: 'Please try again',
					confirmButtonColor: '#2a454e'
				})
			}
		})
	
	}

	function confirmation(event){

		if(quantity === 0 || quantity === null || quantity === undefined || quantity === '' || quantity == 0 || quantity === '0' || quantity < 0){

			Swal2.fire({
				title: "Error",
				icon: "error",
				text: "Quantity cannot be negative, empty or zero.",
				confirmButtonColor: '#2a454e'
			})
		} else if(user.isAdmin === true){
			Swal2.fire({
				title: "Error",
				icon: "error",
				text: "You are an admin.",
				confirmButtonColor: '#2a454e'
			})
		} else {
			Swal2.fire({
		  title: 'Proceed?',
		  html: `<b>${name}</b><br><br>Quantity: ${quantity}<br><br>Total Amount: $${(price * quantity).toFixed(2)}`,
		  showDenyButton: false,
		  showCancelButton: true,
		  confirmButtonText: 'Checkout',
		  confirmButtonColor: '#2a454e'
		}).then((result) => {
		  /* Read more about isConfirmed, isDenied below */
		  if (result.isConfirmed) {
		    checkout(event);
		  } 
		})

		}

		
	}


	return (
		<Container className="d-flex flex-column vh-50 col-12">
			<Row className="mt-auto mb-auto col-12">
				<Col className="col-12 d-flex">
					<Container className="vh-50 mt-5">
						<Card className="vh-50 mt-5 col-6 me-auto ms-auto mb-5 card">
						      <Card.Body className="d-flex flex-column checkout-card">
						      	<Card.Img variant="top" src={image} className="img-fluid mb-2 ms-auto me-auto text-center" style={{ width: '500px', height: '500px' }}/>
						        <Card.Title className="mt-1">{name}</Card.Title>
						        <Card.Text className="card-description">
						        	{description.split('\n').map((line, index) => (
							            <span key={index}>
							              {line}
							              <br />
							            </span>
							          ))}
						        </Card.Text>
						        <Form.Group className="d-flex flex-column col-2 mb-3">
						        	<Form.Label>
						        	Quantity
						        	</Form.Label>
						        	<Form.Control 
						        	type ="number"
						        	value={quantity}
						        	min={1}
						        	onChange={(event) => setQuantity(event.target.value)}
						        	/>	
						        </Form.Group>
						        <Card.Text className="d-flex flex-row">Price: ${price}</Card.Text>

						        {
						        	user.id === null || user.id === undefined

						        	?

						        	<Button className="mb-2 checkout-btn" as = {Link} to = '/login'>Checkout</Button>

						        	:

						        	<Button className="mb-2 checkout-btn"  onClick={(event) => confirmation(event)}>Checkout</Button>
						        }
						      </Card.Body>
						</Card>
					</Container>
				</Col>
			</Row>
		</Container>
	)
}