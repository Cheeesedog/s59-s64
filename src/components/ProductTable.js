import { Table, Button } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import Swal2 from 'sweetalert2';

export default function ProductTable({ productProp }) {
  const { _id, name, description, price, category, isActive } = productProp;

  const [isItemActive, setIsItemActive] = useState(isActive);
  const [availability, setAvailability] = useState(isActive ? 'Available' : 'Not Available');

  function activate() {
    fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/activate`, {
      method: 'PUT',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        if (data === true) {
          setIsItemActive(true);
          setAvailability('Available');
          Swal2.fire({
            title: 'Activated',
            icon: 'success',
            text: 'Product activation successful.',
            confirmButtonColor: '#464444',
          });
        } else {
          Swal2.fire({
            title: 'Error',
            icon: 'error',
            text: 'Product activation unsuccessful.',
            confirmButtonColor: '#464444',
          });
        }
      });
  }

  function archive() {
    fetch(`${process.env.REACT_APP_API_URL}/products/${_id}/archive`, {
      method: 'PUT',
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        if (data === true) {
          setIsItemActive(false);
          setAvailability('Not Available');
          Swal2.fire({
            title: 'Deactivated',
            icon: 'success',
            text: 'Product deactivation successful.',
            confirmButtonColor: '#464444',
          });
        } else {
          Swal2.fire({
            title: 'Error',
            icon: 'error',
            text: 'Product deactivation unsuccessful.',
            confirmButtonColor: '#464444',
          });
        }
      });
  }

  useEffect(() => {
    setIsItemActive(isActive);
    setAvailability(isActive ? 'Available' : 'Not Available');
  }, [isActive]);

  return (
    <tr>
      <td>{name}</td>
      <td>{description}</td>
      <td>{category}</td>
      <td>${price}</td>
      <td>{availability}</td>
      <td className="text-center text-light">
        <Button as={Link} to={`/admin/update/${_id}`} variant="warning" className="mb-3 bg custom-button text-light">
          Update
        </Button>

        {isItemActive ? (
          <Button variant="danger" className="bg custom-button" onClick={archive}>
            Deactivate
          </Button>
        ) : (
          <Button variant="success" className="bg custom-button" onClick={activate}>
            Activate
          </Button>
        )}
      </td>
    </tr>
  );
}

ProductTable.propTypes = {
  productProp: PropTypes.shape({
    name: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    category: PropTypes.string.isRequired,
    isActive: PropTypes.bool.isRequired,
  }),
};