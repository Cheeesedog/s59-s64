import { Row, Col, Card, Container } from 'react-bootstrap';

export default function Highlights(){
	return(
		<Container className="mb-5">
			<Row className="mt-3 mb-3">
			    <Col xs={12} md={4}>
			        <Card className="cardHighlight p-3">
			            <Card.Body>
			            	<Card.Img src="https://files.pccasegear.com/UserFiles/GLO-GMMK-P75-RGB-B-glorious-gmmk-pro-75-barebone-keyboard-black-slate-product3.jpg" style={{ width: '350px', height: '350px' }}/>
			                <Card.Title className="mt-3">
			                    <h2>Barebone Keyboards</h2>
			                </Card.Title>
			                <Card.Text>
			                    Simplicity meets customization. Explore our barebone keyboards, providing the foundation for your personalized typing experience. Unleash your creativity and build a keyboard tailored to your preferences.
			                </Card.Text>
			            </Card.Body>
			        </Card>
			    </Col>
			    <Col xs={12} md={4}>
			        <Card className="cardHighlight p-3">
			            <Card.Body>
			            	<Card.Img src="https://www.zdnet.com/a/img/resize/39889404491063f1ad92b7cdce326f69cec63774/2022/04/21/9e89b0fb-7113-4e4c-aefc-49e394ea687f/numerous-mechanical-keyboard-switches.jpg?auto=webp&width=1280" style={{ width: '350px', height: '350px' }}/>
			                <Card.Title className="mt-3">
			                    <h2>Switches</h2>
			                </Card.Title>
			                <Card.Text>
			                    Discover the perfect tactile response and typing feel with our premium keyboard switches. From smooth linear switches to satisfying tactile and clicky options, our collection offers a switch for every typing style.
			                </Card.Text>
			            </Card.Body>
			        </Card>
			    </Col>
			    <Col xs={12} md={4}>
			        <Card className="cardHighlight p-3">
			            <Card.Body>
			            	<Card.Img src="https://epomaker.com/cdn/shop/products/EpomakerMatchaKeycaps-11-1000.jpg?v=1643079211" style={{ width: '350px', height: '350px' }}/>
			                <Card.Title className="mt-3">
			                    <h2>Keycaps</h2>
			                </Card.Title>
			                <Card.Text>
			                    Elevate your keyboard aesthetics with our exquisite keycaps collection. From vibrant colors to unique designs, our keycaps allow you to express your style and make a statement.
			                </Card.Text>
			            </Card.Body>
			        </Card>
			    </Col>
			</Row>
		</Container>
	)
}