import { Button, Row, Col, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner(){

	return (
		<Container className="mt-5">
			<Row>
			    <Col className="p-5 banner">
			        <h1>Lazapee</h1>
			        <p className="paragraph">Step into the World of Custom Keyboards at Lazapee - Where Every Keystroke Counts</p>
			        <Button as = {Link} to = '/products' className="btn">See our products</Button>
			    </Col>
			</Row>
		</Container>
	)
}