import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';

import { useContext, useEffect } from 'react';
import {NavLink, Link} from 'react-router-dom';

import UserContext from '../UserContext';

// The as keyword allows components to be treated as if they are different components gaining access to it's properties and functionalities
// The to keyword is used in place of the 'href' for providing the url for the page 
export default function AppNavBar() {

  const {user} = useContext(UserContext);
  // In here, I will capture the value from our localStorage and then store it in a state.
  // Syntax:
    // localStorage.getItem('key/property')

  // console.log(localStorage.getItem('email'));

  // const [user, setUser] = useState(localStorage.getItem('email'));

/*  useEffect(() => {
    setUser(localStorage.getItem('email'))
  }, [localStorage.getItem('email')])*/

  let nav;

  if(user.isAdmin === true){
    nav = "navAdmin";
  } else {
    nav = "nav"
  }

  return (

    <Navbar className={nav}expand="lg" fixed="top">
        <Container fluid >
            <Navbar.Brand as = {Link} to = '/' className="text-light">Lazapee</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav"/>
            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="ms-auto">

                {
                  user.isAdmin === true
                 ?
                  <Nav.Link as = {NavLink} to = '/admin/dashboard' className="text-light">Admin Dashboard</Nav.Link>
                 :
                  <Nav.Link as = {NavLink} to = '/' className="text-light">Home</Nav.Link>
                }
                

                {
                  user.isAdmin === true
                  ?
                  <>
                    <Nav.Link as = {NavLink} to = '/admin/create/product' className="text-light">Create Product</Nav.Link>
                    <Nav.Link as = {NavLink} to = '/products' className="text-light">Products</Nav.Link>
                  </>
                  :

                  <Nav.Link as = {NavLink} to = '/products' className="text-light">Products</Nav.Link>
                }
                


                {
                  user.id === null || user.id === undefined
                  ?
                  <>
                    <Nav.Link as = {NavLink} to = '/register' className="text-light">Sign Up</Nav.Link>
                    <Nav.Link as = {NavLink} to = '/login' className="text-light">Login</Nav.Link>
                  </>
                  
                  :
                  <Nav.Link as = {NavLink} to = '/logout' className="text-light">Logout</Nav.Link>
                }

                </Nav>
            </Navbar.Collapse>
        </Container>
    </Navbar>
  )
}