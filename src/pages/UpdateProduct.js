import { useState, useEffect } from 'react';
import { Container, Row, Col, Form, Button } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal2 from 'sweetalert2';


export default function UpdateProduct(){

	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [category, setCategory] = useState('');
	const [image, setImage] = useState('');

	const [isDisabled, setIsDisabled] = useState(true);

	const {id} = useParams();
	const navigate = useNavigate();



	useEffect(() => {

		fetch(`${process.env.REACT_APP_API_URL}/products/${id}`)
		.then(response => response.json())
		.then(data => {
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setCategory(data.category);
			setImage(data.image);
		})
	}, []);

	function updateProduct(event){
		event.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/products/update/${id}`, {
			method: "PUT",
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				category: category,
				image: image
			})
		})
		.then(response => response.json())
		.then(data => {

			if(data===true){
				Swal2.fire({
					title: "Update Successful",
					icon: "success",
					text: "Successfully updated a product.",
					confirmButtonText: 'OK',
					confirmButtonColor: '#464444'
				}).then(() => {
					navigate('/admin/dashboard')
				})
				
			} else {
				Swal2.fire({
					title: "Error",
					icon: "error",
					text: "Error updating a product.",
					confirmButtonColor: '#464444'
				})
			}
		})

	}

	function clear(){
		setName('');
		setDescription('');
		setPrice('');
		setCategory('');
		setImage('');
	}

	useEffect(() => {

		if(name !== '' && description !== '' && price !== '' && category !== '' && image !== ''){
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}

	}, [name, description, price, category, image])

	useEffect(() => {
	    // Fetch user details and update isAdmin state accordingly
	    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
	      headers: {
	        Authorization: `Bearer ${localStorage.getItem('token')}`,
	      },
	    })
	      .then((response) => response.json())
	      .then((data) => {
	        if (!data.isAdmin || !localStorage.getItem('token')) {
	          // Redirect to the "Not Found" page if user is not an admin or not logged in
	          navigate('*');
	        }
	      });
	  }, []);

	return(

		<Container className="mt-5">
			<Row>
				<Col>
						<h1 className="text-center mt-5">Update Product</h1>
					 	<Form className="col-8 ms-auto me-auto mt-5" onSubmit ={event => updateProduct(event)}>
					 		<Form.Group className="mb-3 mx-auto">
					 			<Form.Label>Product Name</Form.Label>
					 			<Form.Control 
					 				type="text" 
					 				placeholder="Enter Product Name"
					 				value={name}
					 				onChange = {(event) => setName(event.target.value)}
					 			/>
					 		</Form.Group>

					 		<Form.Group className="mb-3" >
					 			<Form.Label>Description</Form.Label>
					 			<Form.Control 
					 				as="textarea" rows={3} 
					 				placeholder="Enter Description"
					 				value={description}
					 				onChange = {(event) => setDescription(event.target.value)}
					 			/>
					 		</Form.Group>

					 		<Form.Group className="mb-3">
					 			<Form.Label>Price</Form.Label>
					 			<Form.Control 
					 				type="number" 
					 				placeholder="Enter Price"
					 				value={price}
					 				onChange={(event) => setPrice(event.target.value)}
					 			/>
					 		</Form.Group>

					 		<Form.Group className="mb-3">
					 			<Form.Label>Category</Form.Label>
					 			<Form.Control 
					 				type="text" 
					 				placeholder="Enter Category"
					 				value={category}
					 				onChange={(event) => setCategory(event.target.value)}
					 			/>
					 		</Form.Group>

					 		<Form.Group className="mb-3">
					 			<Form.Label>Image Link</Form.Label>
					 			<Form.Control 
					 				type="text" 
					 				placeholder="Enter Image Link"
					 				value={image}
					 				onChange={(event) => setImage(event.target.value)}
					 			/>
					 		</Form.Group>

					 		<Form.Group className="d-flex flex-row update-form-button mt-4">
					 			<Button variant="warning"className="ms-auto me-3 bg update-button text-light" type="submit" disabled={isDisabled}>
					 				Update
					 			</Button>
					 			<Button variant="primary" className="ms-3 me-3 bg clear-button text-light" onClick = {clear}>
					 				Clear
					 			</Button>
					 			<Button variant="danger"as = {Link} to = '/admin/dashboard' className="me-auto ms-3 bg cancel-button">
					 				Cancel
					 			</Button>
					 		</Form.Group>
						</Form>
				</Col>
			</Row>
		</Container>

	)
}
