import {Row, Col, Container, Image} from 'react-bootstrap'
import {Link} from 'react-router-dom';
import errorImage from '../components/ErrorImage.jpg';


export default function NotFound(){
	return (
		<Container className="mt-5 h-100">
			<Row>
				<Col className="d-flex flex-column mt-5">
						<h1 className ="text-center mt-auto">Page Not Found</h1>
						<Image src={errorImage} style={{ width: '700px', height: '700px' }} className="ms-auto me-auto"/>
						<p className = "text-center"> Go back to <Link to = '/' className="text-decoration-none">homepage</Link>.</p>
				</Col>
			</Row>
		</Container>
	)
}