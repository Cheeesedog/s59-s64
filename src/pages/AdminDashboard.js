import { Fragment, useState, useEffect, useContext } from 'react';
import { Container, Row, Col, Button, Table } from 'react-bootstrap';
import { Link, useNavigate } from 'react-router-dom';
import ProductTable from '../components/ProductTable'
import UpdateProduct from './UpdateProduct';

import UserContext from '../UserContext';

export default function AdminDashBoard(){

	const { user, setUser } = useContext(UserContext);

	const [products, setProducts] = useState([]);

	const [isAdmin, setIsAdmin] = useState(false);
	const [isLoggedIn, setIsLoggedIn] = useState(false);

	const navigate = useNavigate();

	// We are going to add an useEffect here so that in every time that we refresh our application, it will fetch the updated content of our courses.
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/all`)
		.then(response => response.json())
		.then(data => {
			// console.log(data)
			setProducts(data.map(product => {
				return (
						<ProductTable key={product._id} productProp={product} />
				)
			}))

	});
	}, []);

const [admin, setAdmin] = useState('')

	
useEffect(() => {
    // Fetch user details and update isAdmin state accordingly
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
      headers: {
        Authorization: `Bearer ${localStorage.getItem('token')}`,
      },
    })
      .then((response) => response.json())
      .then((data) => {
        if (!data.isAdmin || !localStorage.getItem('token')) {
          // Redirect to the "Not Found" page if user is not an admin or not logged in
          navigate('*');
        }
      });
  }, []);
 
	return (

			<Container className="d-flex">
				<Row>
					<Col className=" col-button mt-5">
						<h1 className="text-center mt-5">Admin Dashboard</h1>
						<Container className="d-flex admin-button">
							<Button as = {Link} to = '/admin/create/product'className="text-center bg mb-5 ms-auto me-auto">
								Create Product
							</Button>

						</Container>
						
						<Table striped bordered hover>
						      <thead>
						        <tr className="text-center">
						          <th>Name</th>
						          <th>Description</th>
						          <th>Category</th>
						          <th>Price</th>
						          <th>Availability</th>
						          <th>Actions</th>
						        </tr>
						      </thead>
						      <tbody>
						      {products}
						      </tbody>

						</Table>
					</Col>
				</Row>
			</Container>

	)
}