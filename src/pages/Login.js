import { Button, Form, Row, Col, Container } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import { Navigate, useNavigate, Link } from 'react-router-dom';

// sweetalert2 is a simple and useful package for generating user alerts with ReactJS
import Swal2 from 'sweetalert2';

import UserContext from '../UserContext';


export default function Login(){




const navigate = useNavigate();

// State hooks to store the values of the input fields
const [email, setEmail] = useState('');

const [password, setPassword] = useState('');

// State to determine whether submit button is enabled or not
const [isDisabled, setIsDisabled] = useState(true);

// Allows us to consume the UserContext object and it's properties to use for user validation
const { user, setUser } = useContext(UserContext);

// const [user,setUser] = useState(localStorage.getItem('email'))

useEffect(() => {

	if(email !== '' && password !== '' ){
		setIsDisabled(false);
	} else {
		setIsDisabled(true);
	}
}, [email, password])



	function loginUser(event){
		event.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password 
			})
		})
		.then(response => response.json())
		.then(data => {

			// if statement to check whether the login is successful.
			if(data === false){

				// in adding sweetalert2 you have to use the fire method
				Swal2.fire({
					title: "Login Unsuccessful",
					icon: "error",
					text: "Check your login credentials and try again",
					confirmButtonColor: '#2a454e'
				})
			} else {
				localStorage.setItem('token', data.access);
				retrieveUserDetails(data.access);
			}
		})

	}

	const retrieveUserDetails = (token) => {
			fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
				headers: {
					Authorization: `Bearer ${token}`
				}
			})
			.then(response => response.json())
			.then(data => {
				setUser({
					id: data._id,
					isAdmin: data.isAdmin
				})

				if (data.isAdmin) {
				      // User is an admin, perform admin-related actions
				      Swal2.fire({
					      title: "Hello Admin",
					      icon: "success",
					      text: "What should we do today?",
					      confirmButtonColor: '#2a454e'
				      })
				      		
				      navigate('/admin/dashboard')

				    } else {
				      // User is not an admin, perform regular user actions
				      Swal2.fire({
					      title: "Login successful",
					      icon: "success",
					      text: "Happy Browsing!",
					      confirmButtonColor: '#2a454e'
				      })

				      navigate('/products')
      
			}
		})

}

	return(
		user.id === null || user.id === undefined
		?
			<Container className="mt-5">
				<Row className="mt-auto my-auto">
					<Col className = "col-6 mx-auto">
						<h1 className="mt-5 text-center">Login</h1>
						<Form onSubmit = {event => loginUser(event)} >
						    <Form.Group className="mb-3" controlId="formBasicEmail">
						        <Form.Label>Email address</Form.Label>
						        <Form.Control 
						        type="email" 
						        placeholder="Enter email" 
						        value = {email}
						        onChange = {event => setEmail(event.target.value)}
						        />
						    </Form.Group>
						    <Form.Group className="mb-3" controlId="formBasicPassword">
						        <Form.Label>Password</Form.Label>
						        <Form.Control 
						        type="password" 
						        placeholder="Password"
						        value = {password}
						        onChange = {event => setPassword(event.target.value)} 
						        />
						    </Form.Group>
						    <Form.Group className="login d-flex form-login">
							    <Button className = "bg btn login-button" variant="primary" type="submit" disabled = {isDisabled}>
							      	Login
							    </Button>
							    <Form.Text className="ms-auto login login-text mt-1">
							    	Don't have an account yet?
							    </Form.Text>
							    <Button as = {Link} to = '/register'className="bg btn ms-3 sign-up-button">
							    	Sign Up
							    </Button>


						    </Form.Group>
						</Form>
					</Col>
				</Row>
			</Container>

		:

		<Navigate to = '*' />
	)
}