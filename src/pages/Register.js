// import Button from 'react-bootstrap/Button';
// import Form from 'react-bootstrap/Form';

import { Button, Form, Row, Col, Container } from 'react-bootstrap'
// We need to import the useState from react
import { useState, useEffect, useContext } from 'react';

import {Navigate, useNavigate, Link} from 'react-router-dom';

import UserContext from '../UserContext';

import Swal2 from 'sweetalert2';

export default function Register(){

	const navigate = useNavigate();

	const {user} = useContext(UserContext);

	// State hooks to store the values of the input fields

	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');

	const [isDisabled, setIsDisabled] = useState(true);

	// We are going to add/create a state that will declare whether the password1 and password2 is equal
	const [isPasswordMatch, setIsPasswordMatch] = useState(true)


	// This useEffect will disable or enable our sign up button
	useEffect(() => {
		// We are going to add if statement and all the condition that we mention should be satisfied before we enable the sign up button
		if( email !== '' && password1 !== '' && password2 !== '' && password1 === password2 && firstName !== '' && lastName !== '' && mobileNo.length >= 11 && mobileNo !== '' && password1.length >= 8 && password2.length >= 8){
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}
	}, [email, password1, password2, firstName, lastName, mobileNo ]);

	// Function to simulate user registration
	function registerUser(event){
		// Prevent page reloading
		event.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(response => response.json())
		.then(data => {

			if(data === true){
				Swal2.fire({
					title: 'Duplicate email found',
					icon: 'error',
					text: 'Please provide a different email.',
					confirmButtonColor: '#2a454e'
				})
			} else {
				// alert('Thank you for registering!');

				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						mobileNo: mobileNo,
						password: password1,
					})
				})
				.then(response => response.json())
				.then(data => {

					if(data === false){
						Swal2.fire({
							title: 'Registration Unsuccessful',
							icon: 'error',
							text: 'Please try again',
							confirmButtonColor: '#2a454e'
						})
					} else {
						Swal2.fire({
							title: 'Registration Successful',
							icon: 'success',
							text: 'Welcome to Lazapee!',
							confirmButtonColor: '#2a454e'
						}).then(() => {
							navigate('/login')
						})

						
					}
				})
			}
		})


	}

	// useEffect to validate whether the password1 is equal to password2

	useEffect(() => {

		if(password1 === password2){
			setIsPasswordMatch(true);
		} else {
			setIsPasswordMatch(false);
		} 
	}, [password1, password2])



	return (
		user.id === null || user.id === undefined
		? 
			<Container className="mt-5">
				<Row className="mt-auto">
					<Col className = "col-6 mx-auto" >
						<h1 className="text-center mt-5">Sign Up</h1>
						<Form onSubmit = {event => registerUser(event)}>

							<Form.Group className="mb-3" controlId="formBasicEmail">
								<Form.Label>First Name</Form.Label>
								<Form.Control 
								type="text" 
								placeholder="Enter first name"
								value = {firstName}
								onChange = {(event) => setFirstName(event.target.value)}
								/>
							</Form.Group>

							<Form.Group className="mb-3" controlId="formBasicEmail">
								<Form.Label>Last Name</Form.Label>
								<Form.Control 
								type="text" 
								placeholder="Enter last name"
								value = {lastName}
								onChange = {(event) => setLastName(event.target.value)}
								/>
							</Form.Group>

						    <Form.Group className="mb-3" controlId="formBasicEmail">
						    	<Form.Label>Email address</Form.Label>
						    	<Form.Control 
						    	type="email" 
						    	placeholder="Enter email address"
						    	value = {email}
						    	onChange = {(event) => setEmail(event.target.value)}
						    	/>
						    </Form.Group>

						    <Form.Group className="mb-3" controlId="formBasicEmail">
						    	<Form.Label>Mobile Number</Form.Label>
						    	<Form.Control 
						    	type="text" 
						    	placeholder="Enter mobile number"
						    	value = {mobileNo}
								onChange = {(event) => setMobileNo(event.target.value)}
						    	/>
						    </Form.Group>

						    <Form.Group className="mb-3" controlId="formBasicPassword1">
						       <Form.Label>Password</Form.Label>
						       <Form.Control 
						       type="password" 
						       placeholder="Enter password"
						       value = {password1}
						       onChange = {event => setPassword1(event.target.value)}
						       />
						    </Form.Group>

						    <Form.Group className="mb-3" controlId="formBasicPassword2">
						        <Form.Label>Confirm Password</Form.Label>
						        <Form.Control 
						        type="password" 
						        placeholder="Confirm your password" 
						        value = {password2}
						        onChange = { event => setPassword2(event.target.value)}
						        />
						        <Form.Text className="text-danger" hidden={isPasswordMatch}>
						              The password does not match!
						        </Form.Text>
						     </Form.Group>

						     <Form.Group className="d-flex form-register">
							    <Button className="bg register-button" type="submit" disabled = {isDisabled}>
							        Sign Up
							    </Button>
							    <Form.Text className="ms-auto mt-1 form-text ">
							     	Already have an account?
							    </Form.Text>
							    <Button as = {Link} to ='/login' className="ms-3 bg login-button">
							        Login
							    </Button>
						     </Form.Group>

						</Form>
					</Col>
				</Row>
			</Container>
		:

		<Navigate to = '*' />
	);
};