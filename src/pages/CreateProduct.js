import { Container, Row, Col, Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import {Navigate, Link, useNavigate} from 'react-router-dom';
import Swal2 from 'sweetalert2';

import UserContext from '../UserContext';



export default function CreateProduct(){

	const { user, setUser } = useContext(UserContext);


	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [category, setCategory] = useState('');
	const [image, setImage] = useState('');

	const [isDisabled, setIsDisabled] = useState(true);

	const navigate = useNavigate();

	useEffect(() => {
		if(name !== '' && description !== '' && price !== '' && category !== '' && image !== ''){
			setIsDisabled(false);
		} else {
			setIsDisabled(true);
		}
	}, [name, description, price, category, image])


	function createProduct(event){
		event.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/products/createProduct`, {
			method: "POST",
			headers: {
				Authorization: `Bearer ${localStorage.getItem('token')}`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				name: name,
				description: description,
				price: price,
				category: category,
				image: image
			})
		})
		.then(response => response.json())
		.then(data => {

			if(data === true){

				Swal2.fire({
					title: "Created Successfully",
					icon: "success",
					text: "Successfully created a product.",
					confirmButtonText: 'OK',
					confirmButtonColor: '#464444'
				}).then(() => {
					navigate('/admin/dashboard')
				})


			} else {

				Swal2.fire({
					title: "Error",
					icon: "error",
					text: "Error creating a product.",
					confirmButtonColor: '#464444'
				})
				
			}
		})

	}

	function clear(){
		setName('');
		setDescription('');
		setPrice('');
		setCategory('');
		setImage('');
	}

	useEffect(() => {
	    // Fetch user details and update isAdmin state accordingly
	    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
	      headers: {
	        Authorization: `Bearer ${localStorage.getItem('token')}`,
	      },
	    })
	      .then((response) => response.json())
	      .then((data) => {
	        if (!data.isAdmin || !localStorage.getItem('token')) {
	          // Redirect to the "Not Found" page if user is not an admin or not logged in
	          navigate('*');
	        }
	      });
	  }, []);


	return(

			<Container>
				<Row>
					<Col className="mt-5">
						<h1 className="text-center mt-5 mb-3">Create Product</h1>
						<Form className="col-8 ms-auto me-auto mt-5 " onSubmit = {event => createProduct(event)}>

						      <Form.Group className="mb-3">
						        <Form.Label>Product Name</Form.Label>
						        <Form.Control 
						        type="text" 
						        placeholder="Enter Product Name"
						        value={name}
						        onChange = {(event) => setName(event.target.value)}
						        />
						      </Form.Group>

						      <Form.Group className="mb-3" >
						        <Form.Label>Description</Form.Label>
						        <Form.Control 
						        as="textarea" rows={3} 
						        placeholder="Enter Description"
						        value={description}
						        onChange={(event) => setDescription(event.target.value)}
						        />
						      </Form.Group>

						      <Form.Group className="mb-3">
						        <Form.Label>Price</Form.Label>
						        <Form.Control 
						        type="number" 
						        placeholder="Enter Price"
						        value={price}
						        onChange={(event) => setPrice(event.target.value)}
						        />
						      </Form.Group>
						      <Form.Group className="mb-3">
						        <Form.Label>Category</Form.Label>
						        <Form.Control 
						        type="text" 
						        placeholder="Enter Category"
						        value={category}
						        onChange={(event) => setCategory(event.target.value)}
						        />
						      </Form.Group>
						      <Form.Group className="mb-3">
						        <Form.Label>Image Link</Form.Label>
						        <Form.Control 
						        type="text" 
						        placeholder="Enter Image Link"
						        value={image}
						        onChange={(event) => setImage(event.target.value)}
						        />
						      </Form.Group>
						      <Form.Group className="d-flex create-button mt-4">
						      	<Button variant="warning" type="submit"className="ms-auto me-3 bg text-light" disabled={isDisabled}>Create</Button>
						      	<Button variant="primary" className="ms-3 me-3 bg" onClick={clear}>Clear</Button>
						      	<Button variant="danger" as = {Link} to = '/admin/dashboard'className="me-auto ms-3 bg">Cancel</Button>
						      </Form.Group>
						</Form>
					</Col>
				</Row>
			</Container>

	)
}