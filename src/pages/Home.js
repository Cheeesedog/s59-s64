import { Fragment, useContext } from 'react';
import { Container } from 'react-bootstrap';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

import UserContext from '../UserContext';


export default function Home(){

	const { user } = useContext(UserContext);
	return (
		<Fragment>
			<Container className="mt-5">
				<Banner />
				<Highlights />
			</Container>
		</Fragment>
	)
}