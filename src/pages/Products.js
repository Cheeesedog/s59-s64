import { useState, useEffect } from 'react';
import ProductCard from '../components/ProductCard';
import UpdateProduct from './UpdateProduct';
import { Row, Container } from 'react-bootstrap';


export default function Products(){
	// Check to see if the mock data was captured
	// console.log(coursesData);

	const [products, setProducts] = useState([]);

	// We are going to add an useEffect here so that in every time that we refresh our application, it will fetch the updated content of our courses.
	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/`)
		.then(response => response.json())
		.then(data => {
			// console.log(data)
			setProducts(data.map(product => {
				return (
					<ProductCard key={product._id} productProp={product} />

				)
			}))

	});
	}, []);

	// The "map" method loops through the individual course objects in our array and returns a component for each course
	// Multiple components created through the map method must have a unique key that will help React JS identify which components.elements have been changed
	// Everytime the map method loops through the data, it creates a "CourseCard" component and then passes the current element in our courseData array using the courseProp
	/*const courses = coursesData.map(course => {
		return (
			<CourseCard key={course.id} courseProp={course} />

		)
	})*/


	// The course in the CourseCard component is called a prop which is a shorthand for "property" since components are considered as objects in ReactJS
	// The curly braces ({}) are used for props to signify that we are providing information using JavaScript expressions rather than hard coded values which use double quotes ""
	// We can pass information from one component to another using props. This is reffered to as props drilling
	return (
		<Container className="d-flex flex-column pb-5 mt-5">
			<h1 className = "text-center mt-5 mb-3">Products</h1>
			<Row className="mt-auto">
				{products}
			</Row>
		</Container>
	)
}