import React from 'react';

// Create context object
// A context object as the name states is a data type of an object that can be used to store information that can be shared within the app.
// The context object is a different approach of passing information between components and allows easier access avoiding the use of props.

// Using the createContext from react we can create a context in our app
// We contained the context create in our UserContext variable
// We named it UserContext simply because this context will contain the information of our user.
const UserContext = React.createContext();

// The provider component allows other component to consume/use the context object and supply the neccessary information needed to the context object
export const UserProvider = UserContext.Provider;

export default UserContext;